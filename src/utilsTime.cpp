//
// Created by Andrei on 27.08.21.
//

#include <AndreiUtils/utilsTime.h>
#include <AndreiUtils/utilsString.h>
#include <cassert>
#include <iomanip>
#include <sstream>

using namespace AndreiUtils;
using namespace std;
using namespace std::chrono;

typedef ratio<3600 * 24> daysRatio;
typedef ratio<3600> hoursRatio;
typedef ratio<60> minutesRatio;

AndreiUtils::HighResTimePoint AndreiUtils::nowHighResClock() {
    return AndreiUtils::HighResClock::now();
}

AndreiUtils::SteadyTimePoint AndreiUtils::nowSteadyClock() {
    return AndreiUtils::SteadyClock::now();
}

AndreiUtils::SystemTimePoint AndreiUtils::nowSystemClock() {
    return AndreiUtils::SystemClock::now();
}

AndreiUtils::SystemTimePoint AndreiUtils::now() {
    return nowSystemClock();
}

string AndreiUtils::convertChronoToString(SystemTimePoint const &time, string const &format) {
    time_t timeStruct;
    timeStruct = system_clock::to_time_t(time);
    stringstream ss;
    // ss << put_time(localtime(&timeStruct), format.c_str());
    ss << put_time(gmtime(&timeStruct), format.c_str());
    return ss.str();
}

SystemTimePoint AndreiUtils::convertStringToChrono(string const &time, string const &format) {
    tm tm = {};
    stringstream ss(time);
    ss >> get_time(&tm, format.c_str());
    // return system_clock::from_time_t(mktime(&tm));
    #if defined(WIN32) || defined(WIN64)
    return system_clock::from_time_t(_mkgmtime(&tm));
    #else
    return system_clock::from_time_t(timegm(&tm));
    #endif
}

string AndreiUtils::convertChronoToStringWithSubsecondsCustomJoin(SystemTimePoint const &time, string const &joiner) {
    return convertChronoToStringWithSubseconds(time, "%Y-%m-%d-%H-%M-%S", "%ns", joiner);
}

SystemTimePoint AndreiUtils::convertStringToChronoWithSubsecondsCustomJoin(string const &time, string const &joiner) {
    return convertStringToChronoWithSubseconds(time, "%Y-%m-%d-%H-%M-%S", "%ns", joiner);
}

string AndreiUtils::convertChronoToStringWithSubseconds(SystemTimePoint const &time, string const &format,
                                                        string const &subsecondFormat, string const &joiner) {
    if (joiner.empty()) {
        AndreiUtils::myWarning("The subsecond joiner string is empty... "
                               "Reconstructing the original time from the string will not work!");
    } else if (AndreiUtils::contains(format, joiner)) {
        AndreiUtils::myWarning("The subsecond joiner string is contained in the time formatter... "
                               "Reconstructing the original time from the string will not work!");
    }
    string res = convertChronoToString(time, format);
    string subsecondRes = subsecondFormat;
    if (!subsecondRes.empty()) {
        if (!joiner.empty() && AndreiUtils::contains(subsecondFormat, joiner)) {
            AndreiUtils::myWarning("The subsecond joiner string is contained in the subsecond time formatter... "
                                   "Reconstructing the original time from the string will not work!");
        }
        auto d = time.time_since_epoch();
        auto seconds = std::chrono::duration_cast<std::chrono::seconds>(d);
        d -= seconds;  // now we are in the subsecond time interval
        auto fullNanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(d).count();
        subsecondRes = AndreiUtils::replace(subsecondRes, "%ns",
                                            AndreiUtils::padLeftUntil(to_string(fullNanoseconds), "0", 9));
        subsecondRes = AndreiUtils::replace(subsecondRes, "%pns",
                                            AndreiUtils::padLeftUntil(to_string(fullNanoseconds % 1000), "0", 3));
        fullNanoseconds /= 1000;
        subsecondRes = AndreiUtils::replace(subsecondRes, "%us",
                                            AndreiUtils::padLeftUntil(to_string(fullNanoseconds), "0", 6));
        subsecondRes = AndreiUtils::replace(subsecondRes, "%pus",
                                            AndreiUtils::padLeftUntil(to_string(fullNanoseconds % 1000), "0", 3));
        fullNanoseconds /= 1000;
        subsecondRes = AndreiUtils::replace(subsecondRes, "%ms",
                                            AndreiUtils::padLeftUntil(to_string(fullNanoseconds), "0", 3));
    }
    return res + joiner + subsecondRes;
}

SystemTimePoint AndreiUtils::convertStringToChronoWithSubseconds(
        string const &time, string const &format, string const &subsecondFormat, string const &joiner) {
    if (joiner.empty()) {
        string message = "The subsecond joiner part is empty... Can not reconstruct subsecond part."
                         "Will reconstruct only using format = " + format;
        AndreiUtils::myWarning(message);
        return convertStringToChrono(time, format);
    }
    auto timeSplits = AndreiUtils::splitString(time, joiner);
    if (timeSplits.size() > 2) {
        AndreiUtils::myWarning("Joiner string \"" + joiner + "\" is contained multiple times in the time \"" +
                               time + "\". Will only use the first two components and try to reconstruct this way...");
    }
    auto res = convertStringToChrono(timeSplits[0], format);
    string timeString = timeSplits[1];
    size_t subsecondFormatSize = subsecondFormat.size(), timeSize = timeString.size();
    // the -3 is because all subsecond formats are at least 3 characters in length
    for (int formatIndex = 0, timeIndex = 0;
         formatIndex < subsecondFormatSize - 2 && timeIndex < timeSize; formatIndex++, timeIndex++) {
        if (subsecondFormat[formatIndex] == timeString[timeIndex] && subsecondFormat[formatIndex] != '%') {
            continue;
        } else if (subsecondFormat[formatIndex] != '%') {
            break;
        }
        assert (subsecondFormat[formatIndex] == '%');
        // until now all indices [formatIndex, formatIndex + 1, formatIndex + 2] are safe to access
        if (subsecondFormat[formatIndex + 1] == '%') {
            if (timeString[timeIndex] == '%') {
                formatIndex++;
                continue;
            } else {
                break;
            }
        } else if (subsecondFormat[formatIndex + 1] == 'p') {
            if (formatIndex + 3 >= subsecondFormatSize) {
                break;
            }
            if (subsecondFormat[formatIndex + 2] == 'u' && subsecondFormat[formatIndex + 3] == 's' &&
                timeIndex + 2 < timeSize) {
                // part microseconds: expecting at three characters more in timeString string
                res += std::chrono::microseconds(stoi(timeString.substr(timeIndex, 3)));
                timeIndex += 2;  // 3 - 1 because there's the timeIndex++ performed in the for-loop
            } else if (subsecondFormat[formatIndex + 2] == 'n' && subsecondFormat[formatIndex + 3] == 's' &&
                       timeIndex + 2 < timeSize) {
                // part nanoseconds: expecting at three characters more in timeString string
                res += std::chrono::nanoseconds(stoi(timeString.substr(timeIndex, 3)));
                timeIndex += 2;  // 3 - 1 because there's the timeIndex++ performed in the for-loop
            } else {
                AndreiUtils::myWarning("Unknown formatter for subsecond values in " + subsecondFormat);
                break;
            }
            formatIndex += 3;
        } else if (subsecondFormat[formatIndex + 1] == 'm' && subsecondFormat[formatIndex + 2] == 's' &&
                   timeIndex + 2 < timeSize) {
            // milliseconds: expecting at three characters more in timeString string
            res += std::chrono::milliseconds(stoi(timeString.substr(timeIndex, 3)));
            timeIndex += 2;  // 3 - 1 because there's the timeIndex++ performed in the for-loop
            formatIndex += 2;
        } else if (subsecondFormat[formatIndex + 1] == 'u' && subsecondFormat[formatIndex + 2] == 's' &&
                   timeIndex + 5 < timeSize) {
            // microseconds: expecting at six characters more in timeString string
            res += std::chrono::microseconds(stoi(timeString.substr(timeIndex, 6)));
            timeIndex += 5;  // 6 - 1 because there's the timeIndex++ performed in the for-loop
            formatIndex += 2;
        } else if (subsecondFormat[formatIndex + 1] == 'n' && subsecondFormat[formatIndex + 2] == 's' &&
                   timeIndex + 8 < timeSize) {
            // nanoseconds: expecting at nine characters more in timeString string
            res += std::chrono::nanoseconds(stoi(timeString.substr(timeIndex, 9)));
            timeIndex += 8;  // 9 - 1 because there's the timeIndex++ performed in the for-loop
            formatIndex += 2;
        } else {
            AndreiUtils::myWarning("Unknown formatter for subsecond values in " + subsecondFormat);
            break;
        }
    }
    return res;
}

chrono::duration<double> convertDurationFromTimeUnit(double t, TimeUnit timeUnit) {
    switch (timeUnit) {
        case DAY: {
            return chrono::duration<double, daysRatio>(t);
            break;
        }
        case HOUR: {
            return chrono::duration<double, hoursRatio>(t);
            break;
        }
        case MINUTE: {
            return chrono::duration<double, minutesRatio>(t);
            break;
        }
        case SECOND: {
            return chrono::duration<double>(t);
            break;
        }
        case MILLISECOND: {
            return chrono::duration<double, milli>(t);
            break;
        }
        case MICROSECOND: {
            return chrono::duration<double, micro>(t);
            break;
        }
        case NANOSECOND: {
            return chrono::duration<double, nano>(t);
            break;
        }
        default : {
            throw runtime_error("Unknown TimeUnit " + to_string(timeUnit));
        }
    }
    /*
    chrono::duration<double> time = chrono::duration<double>(t / getMultiplicationFactorRelativeToSeconds(timeUnit));
    return time;
    //*/
}

SystemTimePoint AndreiUtils::addDeltaTime(SystemTimePoint const &timePoint, double deltaT, string const &timeUnit) {
    return addDeltaTime(timePoint, deltaT, convertStringToTimeUnit(timeUnit));
}

SystemTimePoint AndreiUtils::addDeltaTime(SystemTimePoint const &timePoint, double deltaT, TimeUnit timeUnit) {
    return timePoint + chrono::duration_cast<nanoseconds>(convertDurationFromTimeUnit(deltaT, timeUnit));
}

SystemTimePoint AndreiUtils::getTimePoint(double t, string const &timeUnit) {
    return getTimePoint(t, convertStringToTimeUnit(timeUnit));
}

SystemTimePoint AndreiUtils::getTimePoint(double t, TimeUnit timeUnit) {
    return SystemTimePoint(chrono::duration_cast<nanoseconds>(convertDurationFromTimeUnit(t, timeUnit)));
}

double AndreiUtils::getTime(SystemTimePoint const &t, string const &timeUnit) {
    return getTime(t, convertStringToTimeUnit(timeUnit));
}

double AndreiUtils::getTime(SystemTimePoint const &t, TimeUnit timeUnit) {
    return chrono::duration<double>(t.time_since_epoch()).count() * getMultiplicationFactorRelativeToSeconds(timeUnit);
}

double AndreiUtils::getTimeDiff(SystemTimePoint const &t1, SystemTimePoint const &t2, string const &timeUnit) {
    return getTimeDiff(t1, t2, convertStringToTimeUnit(timeUnit));
}

double AndreiUtils::getTimeDiff(SystemTimePoint const &t1, SystemTimePoint const &t2, TimeUnit timeUnit) {
    return chrono::duration<double>(t1 - t2).count() * getMultiplicationFactorRelativeToSeconds(timeUnit);
}

double AndreiUtils::getTime(double t, string const &timeUnit) {
    return getTime(t, convertStringToTimeUnit(timeUnit));
}

double AndreiUtils::getTime(double t, TimeUnit timeUnit) {
    return t * getMultiplicationFactorRelativeToSeconds(timeUnit);
}

void AndreiUtils::getDateFromTime(struct tm *&t, time_t time, int &year) {
    t = gmtime(&time);
    year = t->tm_year + 1900;
}

void AndreiUtils::getDateFromTime(struct tm *&t, time_t time, int &year, int &month) {
    AndreiUtils::getDateFromTime(t, time, year);
    month = t->tm_mon + 1;
}

void AndreiUtils::getDateFromTime(struct tm *&t, time_t time, int &year, int &month, int &day) {
    AndreiUtils::getDateFromTime(t, time, year, month);
    day = t->tm_mday;
}

void AndreiUtils::getDateFromTime(struct tm *&t, time_t time, int &year, int &month, int &day, int &hour) {
    AndreiUtils::getDateFromTime(t, time, year, month, day);
    hour = t->tm_hour;
}

void AndreiUtils::getDateFromTime(struct tm *&t, time_t time, int &year, int &month, int &day, int &hour, int &min) {
    AndreiUtils::getDateFromTime(t, time, year, month, day, hour);
    min = t->tm_min;
}

void AndreiUtils::getDateFromTime(struct tm *&t, time_t time, int &year, int &month, int &day, int &hour, int &min,
                                  int &sec) {
    AndreiUtils::getDateFromTime(t, time, year, month, day, hour, min);
    sec = t->tm_sec;
}

void AndreiUtils::getDateFromTime(time_t time, int &year, int &month, int &day, int &hour, int &min, int &sec) {
    struct tm *t = nullptr;
    AndreiUtils::getDateFromTime(t, time, year, month, day, hour, min, sec);
}

void AndreiUtils::getDateFromNow(int &year, int &month, int &day, int &hour, int &min, int &sec) {
    auto now = time(nullptr);
    AndreiUtils::getDateFromTime(now, year, month, day, hour, min, sec);
}

void AndreiUtils::getDateFromTime(time_t time, int &year, int &month, int &day) {
    struct tm *t = nullptr;
    AndreiUtils::getDateFromTime(t, time, year, month, day);
}

void AndreiUtils::getDateFromNow(int &year, int &month, int &day) {
    auto now = time(nullptr);
    AndreiUtils::getDateFromTime(now, year, month, day);
}

void AndreiUtils::updateTime(clock_t const &newTime, clock_t &prevTime, bool updatePrevTime, float *difference) {
    assert(updatePrevTime || difference != nullptr);
    if (difference != nullptr) {
        *difference = ((float) (newTime - prevTime)) / CLOCKS_PER_SEC;
    }
    if (updatePrevTime) {
        prevTime = newTime;
    }
}

void AndreiUtils::updateTime(clock_t const &newTime, clock_t &prevTime, bool updatePrevTime, double *difference) {
    assert(updatePrevTime || difference != nullptr);
    if (difference != nullptr) {
        *difference = ((double) (newTime - prevTime)) / CLOCKS_PER_SEC;
    }
    if (updatePrevTime) {
        prevTime = newTime;
    }
}

clock_t AndreiUtils::createDeltaTime(double fps, double deltaSec) {
    if (fps > 0) {
        return (clock_t) (CLOCKS_PER_SEC * 1 / fps);
    }
    return (clock_t) (CLOCKS_PER_SEC * deltaSec);
}
