//
// Created by Andrei on 27.08.21.
//

#include <AndreiUtils/utilsEigenOpenCV.h>

using namespace AndreiUtils;
using namespace Eigen;
using namespace std;

Array2f AndreiUtils::cvPointToEigenArray(cv::Point2f const &point) {
    return {point.x, point.y};
}

Array2d AndreiUtils::cvPointToEigenArray(cv::Point2d const &point) {
    return {point.x, point.y};
}

cv::Point2f AndreiUtils::eigenArrayToCVPoint(Array2f const &array) {
    return {array.x(), array.y()};
}

cv::Point2d AndreiUtils::eigenArrayToCVPoint(Array2d const &array) {
    return {array.x(), array.y()};
}

Array3f AndreiUtils::cvPointToEigenArray(cv::Point3f const &point) {
    return {point.x, point.y, point.z};
}

Array3d AndreiUtils::cvPointToEigenArray(cv::Point3d const &point) {
    return {point.x, point.y, point.z};
}

cv::Point3f AndreiUtils::eigenArrayToCVPoint(Array3f const &array) {
    return {array.x(), array.y(), array.z()};
}

cv::Point3d AndreiUtils::eigenArrayToCVPoint(Array3d const &array) {
    return {array.x(), array.y(), array.z()};
}

Vector2f AndreiUtils::cvPointToEigenVector(cv::Point2f const &point) {
    return {point.x, point.y};
}

Vector2d AndreiUtils::cvPointToEigenVector(cv::Point2d const &point) {
    return {point.x, point.y};
}

cv::Point2f AndreiUtils::eigenVectorToCVPoint(Vector2f const &vector) {
    return {vector.x(), vector.y()};
}

cv::Point2d AndreiUtils::eigenVectorToCVPoint(Vector2d const &vector) {
    return {vector.x(), vector.y()};
}

Vector3f AndreiUtils::cvPointToEigenVector(cv::Point3f const &point) {
    return {point.x, point.y, point.z};
}

Vector3d AndreiUtils::cvPointToEigenVector(cv::Point3d const &point) {
    return {point.x, point.y, point.z};
}

cv::Point3f AndreiUtils::eigenVectorToCVPoint(Vector3f const &vector) {
    return {vector.x(), vector.y(), vector.z()};
}

cv::Point3d AndreiUtils::eigenVectorToCVPoint(Vector3d const &vector) {
    return {vector.x(), vector.y(), vector.z()};
}

Eigen::Vector2f AndreiUtils::cvVectorToEigenVector(cv::Vec2f const &v) {
    return {v[0], v[1]};
}

Eigen::Vector2d AndreiUtils::cvVectorToEigenVector(cv::Vec2d const &v) {
    return {v[0], v[1]};
}

cv::Vec2f AndreiUtils::eigenVectorToCVVector(Eigen::Vector2f const &v) {
    return {v[0], v[1]};
}

cv::Vec2d AndreiUtils::eigenVectorToCVVector(Eigen::Vector2d const &v) {
    return {v[0], v[1]};
}

Eigen::Vector3f AndreiUtils::cvVectorToEigenVector(cv::Vec3f const &v) {
    return {v[0], v[1], v[2]};
}

Eigen::Vector3d AndreiUtils::cvVectorToEigenVector(cv::Vec3d const &v) {
    return {v[0], v[1], v[2]};
}

cv::Vec3f AndreiUtils::eigenVectorToCVVector(Eigen::Vector3f const &v) {
    return {v[0], v[1], v[2]};
}

cv::Vec3d AndreiUtils::eigenVectorToCVVector(Eigen::Vector3d const &v) {
    return {v[0], v[1], v[2]};
}

void AndreiUtils::writeEigenArray(cv::FileStorage &fs, ArrayXf const &x) {
    fs << "{";
    int size = (int) x.size();
    fs << "size" << size;
    for (int i = 0; i < size; i++) {
        fs << "elem_" + std::to_string(i) << x[i];
    }
    fs << "}";
}

void AndreiUtils::readEigenArray(cv::FileNode const &node, ArrayXf &x) {
    int size = (int) node["size"];
    x = ArrayXf::Zero(size);
    for (int i = 0; i < size; i++) {
        x[i] = (float) node["elem_" + std::to_string(i)];
    }
}

void AndreiUtils::write(cv::FileStorage &fs, string const &name, ArrayXf const &x) {
    // internal::WriteStructContext ws(fs, name, FileNode::SEQ + FileNode::FLOW);
    writeEigenArray(fs, x);
}

void AndreiUtils::read(cv::FileNode const &node, ArrayXf &x, ArrayXf const &default_value) {
    if (node.empty()) {
        x = default_value;
    } else {
        readEigenArray(node, x);
    }
}

void AndreiUtils::write(cv::FileStorage &fs, string const &name, Array<float, 3, 1> const &x) {
    write(fs, name, (ArrayXf const &) x);
}

void AndreiUtils::read(cv::FileNode const &node, Array<float, 3, 1> &x, Array<float, 3, 1> const &default_value) {
    read(node, (ArrayXf &) x, default_value);
}

Eigen::Matrix4d AndreiUtils::recoverMatPoseFrom2dAnd3dPoints(
        vector<cv::Point2f> const &points2d, vector<cv::Point3f> const &points3d, double fx, double fy, double ppx,
        double ppy, float distortionCoefficients[5]) {
    cv::Mat tvec;
    cv::Matx33d r;
    recoverPoseFrom2dAnd3dPoints(tvec, r, points2d, points3d, fx, fy, ppx, ppy, distortionCoefficients);

    Eigen::Matrix3d wRo;
    wRo << r(0, 0), r(0, 1), r(0, 2), r(1, 0), r(1, 1), r(1, 2), r(2, 0), r(2, 1), r(2, 2);

    Eigen::Matrix4d T;
    T.topLeftCorner(3, 3) = wRo;
    T.col(3).head(3) << tvec.at<double>(0), tvec.at<double>(1), tvec.at<double>(2);
    T.row(3) << 0, 0, 0, 1;

    return T;
}

Eigen::Matrix4d AndreiUtils::recoverMatPoseFrom2dAnd3dPoints(
        vector<Vector2f> const &eigenPoints2d, vector<Vector3f> const &eigenPoints3d, double fx, double fy, double ppx,
        double ppy, float distortionCoefficients[5]) {
    size_t n = eigenPoints3d.size();
    std::vector<cv::Point2f> points2d(n);
    std::vector<cv::Point3f> points3d(n);
    for (int i = 0; i < n; i++) {
        points2d[i].x = eigenPoints2d[i].x();
        points2d[i].y = eigenPoints2d[i].y();
        points3d[i].x = eigenPoints3d[i].x();
        points3d[i].y = eigenPoints3d[i].y();
        points3d[i].z = eigenPoints3d[i].z();
    }
    return recoverMatPoseFrom2dAnd3dPoints(points2d, points3d, fx, fy, ppx, ppy, distortionCoefficients);
}

Eigen::Matrix4d AndreiUtils::recoverMatPoseFrom2dAnd3dPoints(
        vector<cv::Point2f> const &points2d, vector<cv::Point3f> const &points3d,
        CameraIntrinsicParameters const &intrinsics) {
    return recoverMatPoseFrom2dAnd3dPoints(points2d, points3d, intrinsics.fx, intrinsics.fy, intrinsics.ppx,
                                           intrinsics.ppy, intrinsics.distortionCoefficients);
}

Eigen::Matrix4d AndreiUtils::recoverMatPoseFrom2dAnd3dPoints(
        vector<Vector2f> const &eigenPoints2d, vector<Vector3f> const &eigenPoints3d,
        CameraIntrinsicParameters const &intrinsics) {
    return recoverMatPoseFrom2dAnd3dPoints(eigenPoints2d, eigenPoints3d, intrinsics.fx, intrinsics.fy, intrinsics.ppx,
                                           intrinsics.ppy, intrinsics.distortionCoefficients);
}

DualQuaternion<double> AndreiUtils::recoverPoseFrom2dAnd3dPoints(
        vector<cv::Point2f> const &points2d, vector<cv::Point3f> const &points3d, double fx, double fy, double ppx,
        double ppy, float distortionCoefficients[5]) {
    cv::Mat tvec;
    cv::Matx33d r;
    recoverPoseFrom2dAnd3dPoints(tvec, r, points2d, points3d, fx, fy, ppx, ppy, distortionCoefficients);

    Eigen::Matrix3d wRo;
    wRo << r(0, 0), r(0, 1), r(0, 2), r(1, 0), r(1, 1), r(1, 2), r(2, 0), r(2, 1), r(2, 2);

    return {qFromRotationMatrix(wRo), {tvec.at<double>(0), tvec.at<double>(1), tvec.at<double>(2)}};
}

DualQuaternion<double> AndreiUtils::recoverPoseFrom2dAnd3dPoints(
        vector<Vector2f> const &eigenPoints2d, vector<Vector3f> const &eigenPoints3d, double fx, double fy, double ppx,
        double ppy, float distortionCoefficients[5]) {
    size_t n = eigenPoints3d.size();
    std::vector<cv::Point2f> points2d(n);
    std::vector<cv::Point3f> points3d(n);
    for (int i = 0; i < n; i++) {
        points2d[i].x = eigenPoints2d[i].x();
        points2d[i].y = eigenPoints2d[i].y();
        points3d[i].x = eigenPoints3d[i].x();
        points3d[i].y = eigenPoints3d[i].y();
        points3d[i].z = eigenPoints3d[i].z();
    }
    return recoverPoseFrom2dAnd3dPoints(points2d, points3d, fx, fy, ppx, ppy, distortionCoefficients);
}

DualQuaternion<double> AndreiUtils::recoverPoseFrom2dAnd3dPoints(
        vector<cv::Point2f> const &points2d, vector<cv::Point3f> const &points3d,
        CameraIntrinsicParameters const &intrinsics) {
    return recoverPoseFrom2dAnd3dPoints(points2d, points3d, intrinsics.fx, intrinsics.fy, intrinsics.ppx,
                                        intrinsics.ppy, intrinsics.distortionCoefficients);
}

DualQuaternion<double> AndreiUtils::recoverPoseFrom2dAnd3dPoints(
        vector<Vector2f> const &eigenPoints2d, vector<Vector3f> const &eigenPoints3d,
        CameraIntrinsicParameters const &intrinsics) {
    return recoverPoseFrom2dAnd3dPoints(eigenPoints2d, eigenPoints3d, intrinsics.fx, intrinsics.fy, intrinsics.ppx,
                                        intrinsics.ppy, intrinsics.distortionCoefficients);
}
