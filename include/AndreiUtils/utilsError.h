//
// Created by Andrei on 06.12.21.
//

#ifndef ANDREIUTILS_UTILSERROR_H
#define ANDREIUTILS_UTILSERROR_H

#include <string>

namespace AndreiUtils {
    void error(const char *s);

    void error(std::string const &s);
}

#endif //ANDREIUTILS_UTILSERROR_H
