//
// Created by Andrei on 16.08.24.
//

#pragma once

#include <limits>

namespace AndreiUtils {
    extern double const DOUBLE_INF;
    extern double const DOUBLE_INF_NEG;
    extern long const LONG_INF;
    extern long const LONG_INF_NEG;
}